package us.sensornet.popularmovieappv3_recycler;

import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.List;

import us.sensornet.popularmovieappv3_recycler.Adapter.MovieAdapter;
import us.sensornet.popularmovieappv3_recycler.Api.Service;
import us.sensornet.popularmovieappv3_recycler.Api.ServiceInterface;
import us.sensornet.popularmovieappv3_recycler.Database.FavoritesDbContract;
import us.sensornet.popularmovieappv3_recycler.Model.MovieListModel;
import us.sensornet.popularmovieappv3_recycler.Model.MovieModel;
import us.sensornet.popularmovieappv3_recycler.Model.MovieReviewModel;
import us.sensornet.popularmovieappv3_recycler.Model.MovieTrailerModel;


public class MainActivity extends AppCompatActivity implements ServiceInterface, MovieAdapter.GridItemClickListener {
    @SuppressWarnings("SpellCheckingInspection")
    private static final String RECYCLER_LAYOUT_KEY = "recyclerview_layout";
    private static Bundle mLayoutBundle;
    @SuppressWarnings("unused")
    private final String TAG = getClass().getSimpleName();
    private final String SEARCH_MODE_KEY = "search_mode";
    //private final String GRID_VIEW_STATE_KEY = "gridState";
    private List<MovieModel> mMovies;
    private List<MovieTrailerModel> mTrailers;
    private Service mApiService;
    @SuppressWarnings("FieldCanBeLocal")
    private MovieAdapter mMovieAdapter;
    private String mMode = Service.QueryModes.MostPopular;
    private RecyclerView mRecyclerView;
    @SuppressWarnings("FieldCanBeLocal")
    private RecyclerView.LayoutManager mLayoutManager;
    @SuppressWarnings({"FieldCanBeLocal", "unused"})
    private Parcelable savedRecyclerLayoutState;
    

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main_activity_recycler_view);


        mRecyclerView = findViewById(R.id.rv_main_container);
        mRecyclerView.setHasFixedSize(true);

        // use a Grid layout manager
        mLayoutManager = new GridLayoutManager(this, 2);
        mRecyclerView.setLayoutManager(mLayoutManager);

        // Note the MovieAdapter will have no data
        // until the Service returns from the Api call
        mMovieAdapter = new MovieAdapter(this, this);

        mApiService = new Service(this, false);
        // Add this activity as listener
        mApiService.addListener(this);
        // Call the apiService
        mApiService.popularMovieRequest(mMode);

    }

    // Save instance state
    @SuppressWarnings("EmptyMethod")
    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putParcelable(RECYCLER_LAYOUT_KEY, mLayoutManager.onSaveInstanceState());
        outState.putString(SEARCH_MODE_KEY, mMode);
    }

    // Restore user experience
    @SuppressWarnings("EmptyMethod")
    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        //restore recycler view at same position
        if (savedInstanceState != null) {
            savedRecyclerLayoutState = savedInstanceState.getParcelable(RECYCLER_LAYOUT_KEY);
            mMode = savedInstanceState.getString(SEARCH_MODE_KEY, Service.QueryModes.MostPopular);
        }
    }


    @Override
    public void onResume() {
        super.onResume();
        if (null != mLayoutBundle) {
            mLayoutManager.onRestoreInstanceState(mLayoutBundle.getParcelable(RECYCLER_LAYOUT_KEY));
        }
    }

    @Override
    public void onPause() {
        super.onPause();
        if (null == mLayoutBundle) {
            mLayoutBundle = new Bundle();
        }
        mLayoutBundle.putParcelable(RECYCLER_LAYOUT_KEY, mLayoutManager.onSaveInstanceState());
    }

    // Creates option menu items
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.main_menu, menu);
        return true;
    }

    // Handle option menu items
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.mnu_item_top_rated:
                mMode = Service.QueryModes.TopRated;
                mApiService.popularMovieRequest(mMode);
                return true;

            case R.id.mnu_item_most_popular:
                mMode = Service.QueryModes.MostPopular;
                mApiService.popularMovieRequest(mMode);
                return true;

//            case R.id.mnu_item_favorites:
//                mMode = Service.QueryModes.Favorites;
//                Cursor favoritesCursor = getContentResolver().query(FavoritesDbContract.FAVORITES_CONTENT_URI,
//                        null, null, null,null);
//                //return favoritesCursor;
//                return true;

            default:
                return super.onOptionsItemSelected(item);
        }
    }


    // Load Favorites
    private void loadFavorites(){
        mMovieAdapter.clearData();

        List<MovieModel> movies = new ArrayList<>();

        Cursor favoriteMoviesCursor = getContentResolver().query(FavoritesDbContract.FAVORITES_CONTENT_URI,
                null, null, null, null);

        // Loop over all items in the Cursor and add to movie list
        while(favoriteMoviesCursor.moveToNext()){
            MovieModel model = new MovieModel();

//            model.setId(favoriteMoviesCursor.getInt(favoriteMoviesCursor.getColumnIndex(FavoritesDbContract.Favorites.COLUMN_MOVIE_ID)));
//            //model.setMovieId(favoriteMoviesCursor.getInt(favoriteMoviesCursor.getColumnIndex(FavoritesDbContract.Favorites.COLUMN_MOVIE_ID)));
//            model.setTitle(favoriteMoviesCursor.getString(favoriteMoviesCursor.getColumnIndex(FavoritesDbContract.Favorites.COLUMN_TITLE)));
//            model.setReleaseDate(favoriteMoviesCursor.getString(favoriteMoviesCursor.getColumnIndex(FavoritesDbContract.Favorites.COLUMN_RELEASE_DATE)));
//
//            //model.setGenreIds(favoriteMoviesCursor.getString(favoriteMoviesCursor.getColumnIndex(FavoritesDbContract.Favorites.COLUMN_GENRES)));
//            //model.setAdult(favoriteMoviesCursor.getInt(favoriteMoviesCursor.getColumnIndex(FavoritesDbContract.Favorites.COLUMN_ADULT)));
//            model.setOverview(favoriteMoviesCursor.getString(favoriteMoviesCursor.getColumnIndex(FavoritesDbContract.Favorites.COLUMN_OVERVIEW)));
//            model.setVoteCount(favoriteMoviesCursor.getInt(favoriteMoviesCursor.getColumnIndex(FavoritesDbContract.Favorites.COLUMN_VOTE_COUNT)));
//            model.setVoteAverage(favoriteMoviesCursor.getFloat(favoriteMoviesCursor.getColumnIndex(FavoritesDbContract.Favorites.COLUMN_AVERAGE_VOTE)));
//            model.setPopularity(favoriteMoviesCursor.getFloat(favoriteMoviesCursor.getColumnIndex(FavoritesDbContract.Favorites.COLUMN_POPULARITY)));
//            model.setPosterPath(favoriteMoviesCursor.getString(favoriteMoviesCursor.getColumnIndex(FavoritesDbContract.Favorites.COLUMN_POSTER_PATH)));
//            //model.setVideo(favoriteMoviesCursor.getInt(favoriteMoviesCursor.getColumnIndex(FavoritesDbContract.Favorites.COLUMN_VIDEO)) == 1 ? true:false);
//            model.setOriginalLanguage(favoriteMoviesCursor.getString(favoriteMoviesCursor.getColumnIndex(FavoritesDbContract.Favorites.COLUMN_ORIGINAL_LANGUAGE)));
//            model.setOriginalTitle(favoriteMoviesCursor.getString(favoriteMoviesCursor.getColumnIndex(FavoritesDbContract.Favorites.COLUMN_ORIGINAL_TITLE)));
//            model.setBackdropPath(favoriteMoviesCursor.getString(favoriteMoviesCursor.getColumnIndex(FavoritesDbContract.Favorites.COLUMN_BACKDROP_PATH)));
//            //model.setLastUpdated(favoriteMoviesCursor.getString(favoriteMoviesCursor.getColumnIndex(FavoritesDbContract.Favorites.COLUMN_LAST_UPDATED)));
//            //model.setFavorite(favoriteMoviesCursor.getInt(favoriteMoviesCursor.getColumnIndex(FavoritesDbContract.Favorites.COLUMN_IS_FAVORITE)));

            movies.add(model);
        }

        mMovies = movies;
        mMovieAdapter.setMovieData(movies);
        mRecyclerView.setAdapter(mMovieAdapter);
    }


    // Launches the Detail Activity
    private void showDetails(MovieModel movie) {
        if (null == movie) {
            String msg = getString(R.string.illegal_argument_exception);
            throw new IllegalArgumentException(msg);
        }

        Intent detailIntent = new Intent(this, DetailActivity.class);
        Gson gson = new Gson();
        String jsonMovie = gson.toJson(movie);
        detailIntent.putExtra("jsonMovie", jsonMovie);
        startActivity(detailIntent);
    }


    @Override
    public void onGridItemClick(View view) {
        mMovieAdapter.setMovieData(mMovies);
        int movieId = (int) view.getTag();
        showDetails(mMovieAdapter.getMovieById(movieId));
    }


    //===================================================
    // Methods below are callbacks for ServiceInterface
    // These methods are part of the api service
    // interface and must be provided for callback
    // even if you do not intend to use them.
    //===================================================
    @Override
    public void OnMovieListReady(MovieListModel movieList) {
        //mMovies = movieList.getResults();
        //mMovieAdapter = new MovieAdapter(this, mMovies, this);
    }

    @Override
    public void OnMovieListDataReady(List<MovieModel> movies) {
        mMovieAdapter.clearData();
        if (null == movies) {
            String msg = getString(R.string.illegal_argument_exception);
            throw new IllegalArgumentException(msg);
        }
        mMovies = movies;
        // We can't set the adapter until we have data from the api service
        mMovieAdapter.setMovieData(movies);
        mRecyclerView.setAdapter(mMovieAdapter);
    }

    @Override
    public void OnMovieListDataFailure(Throwable t) {
        Toast.makeText(this, t.getMessage(), Toast.LENGTH_SHORT).show();
    }


    @Override
    public void OnMovieTrailersDataReady(List<MovieTrailerModel> trailerList) {

    }

    @Override
    public void OnMovieTrailersDataFailure(Throwable t) {

    }

    @Override
    public void OnMovieReviewDataReady(List<MovieReviewModel> trailerList) {

    }

    @Override
    public void OnMovieReviewDataFailure(Throwable t) {

    }
}
