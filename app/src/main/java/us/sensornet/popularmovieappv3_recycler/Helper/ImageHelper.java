package us.sensornet.popularmovieappv3_recycler.Helper;

import android.widget.ImageView;

import com.squareup.picasso.Picasso;


public class ImageHelper {
    private static final String IMG_BASE_URL = "http://image.tmdb.org/t/p/";


    public static void getDrawableFromNetwork(ImageView target, String url) {
        Picasso picasso = Picasso.get();
        //picasso.setIndicatorsEnabled(true);
        String imageUrl = IMG_BASE_URL + url;
        picasso.load(imageUrl).placeholder(android.R.drawable.ic_menu_report_image).error(android.R.drawable.stat_notify_error).into(target);
    }

    @SuppressWarnings("unused")
    public class ImageWidth {
        public static final String w92 = "w92";
        public static final String w154 = "w154";
        public static final String w185 = "w185";
        public static final String w342 = "w342";
        public static final String w500 = "w500";
        public static final String w780 = "w780";
    }
}
