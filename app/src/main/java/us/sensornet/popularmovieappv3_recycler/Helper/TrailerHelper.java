package us.sensornet.popularmovieappv3_recycler.Helper;

import android.util.Log;
import android.widget.ImageView;

import com.squareup.picasso.Picasso;


public class TrailerHelper {
    private static final String TAG = "TrailerHelper";

    private static final String IMG_BASE_URL = "https://i.ytimg.com/vi/%s/default.jpg";//"https://img.youtube.com/vi/%s/1.jpg";

    public static void getDrawableFromNetwork(ImageView target, String imgId, Boolean isDebugging) {
        Log.d(TAG, "TrailerHelper: called");
        Picasso picasso = Picasso.get();
        if(isDebugging) {
            picasso.setIndicatorsEnabled(true);
            picasso.setLoggingEnabled(true);
        }

        String imageUrl = String.format(IMG_BASE_URL, imgId);
        picasso.load(imageUrl).placeholder(android.R.drawable.ic_menu_report_image).fit().error(android.R.drawable.stat_notify_error).into(target);
    }

}
