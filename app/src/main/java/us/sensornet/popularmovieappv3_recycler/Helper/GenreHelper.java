package us.sensornet.popularmovieappv3_recycler.Helper;

import java.util.ArrayList;
import java.util.List;

import us.sensornet.popularmovieappv3_recycler.Model.GenreModel;


/* TODO Note: This class will eventually be replaced by an
 * initial call to the api to get the current list of
 * genre ids. For now, we hard code them.
 */
@SuppressWarnings("unused")
public class GenreHelper {

    private final List<GenreModel> mGenres = new ArrayList<>();

    public GenreHelper() {
        loadData();
    }

    public GenreModel getGenreById(int id) {
        for (GenreModel genre : mGenres) {
            if (genre.mId == id) {
                return genre;
            }
        }

        return null;
    }


    public GenreModel getGenreByName(String name) {
        for (GenreModel genre : mGenres) {
            if (genre.mName.equals(name)) {
                return genre;
            }
        }

        return null;
    }


    public GenreModel getGenreByIndex(int idx) {
        return mGenres.get(idx);
    }


    private void loadData() {
        // LOad the static date into the mGenres list
        mGenres.add(new GenreModel(28, "Action"));
        mGenres.add(new GenreModel(12, "Adventure"));
        mGenres.add(new GenreModel(16, "Animation"));
        mGenres.add(new GenreModel(35, "Comedy"));
        mGenres.add(new GenreModel(80, "Crime"));
        mGenres.add(new GenreModel(99, "Documentary"));
        mGenres.add(new GenreModel(18, "Drama"));
        mGenres.add(new GenreModel(10751, "Family"));
        mGenres.add(new GenreModel(14, "Fantasy"));
        mGenres.add(new GenreModel(36, "History"));
        mGenres.add(new GenreModel(27, "Horror"));
        mGenres.add(new GenreModel(10402, "Music"));
        mGenres.add(new GenreModel(9648, "Mystery"));
        mGenres.add(new GenreModel(10749, "Romance"));
        mGenres.add(new GenreModel(878, "Science Fiction"));
        mGenres.add(new GenreModel(10770, "TV Movie"));
        mGenres.add(new GenreModel(53, "Thriller"));
        mGenres.add(new GenreModel(10752, "War"));
        mGenres.add(new GenreModel(37, "Western"));
    }

}