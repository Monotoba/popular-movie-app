package us.sensornet.popularmovieappv3_recycler.YouTube;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.widget.ImageView;

import com.squareup.picasso.OkHttp3Downloader;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.Target;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

import okhttp3.OkHttpClient;

public class YoutubeImageHelper {

    private static final String IMG_BASE_URL = "https://i.ytimg.com/vi/%s/default.jpg";//"https://img.youtube.com/vi/%s/1.jpg";
    private static boolean isDebugMode;
    private final List<YoutubeImageInterface> mListeners;
    private Picasso mPicasso;
    private ArrayList<Target> mTargets;

    private Context mContext;
    private OkHttpClient mClient;

    public YoutubeImageHelper(Context context) {
        mContext = context;
        mPicasso = Picasso.get();
        mListeners = new ArrayList<>();
        mTargets = new ArrayList<>();

    }

    public static boolean getDebugMode() {
        return isDebugMode;
    }

    public static void setDebugMode(boolean mode) {
        isDebugMode = mode;
    }

    public static void getDrawableFromNetwork(Context context, ImageView target, String imgId) {
        Picasso mPicasso;// = Picasso.get();

        /////// Trying long time out
        OkHttpClient mClient = new OkHttpClient.Builder().readTimeout(30000, TimeUnit.MILLISECONDS).build();


        mPicasso = new Picasso.Builder(context).downloader(new OkHttp3Downloader(mClient)).build();

        ///////
        if (isDebugMode) {
            mPicasso.setLoggingEnabled(true);
            mPicasso.setIndicatorsEnabled(true);
        }
        String imageUrl = String.format(IMG_BASE_URL, imgId);
        mPicasso.load(imageUrl).resize(800,640).placeholder(android.R.drawable.ic_menu_report_image).fit().error(android.R.drawable.stat_notify_error).into(target);
    }

    public void addListener(YoutubeImageInterface callback) {
        mListeners.add(callback);
    }

    // This method makes the picasso request using a new target
    // so as not to overwrite any previous requests.
    public void getDrawable(final int index, String imgId) {
        if (isDebugMode) {
            mPicasso.setLoggingEnabled(true);
            mPicasso.setIndicatorsEnabled(true);
        }

        String imageUrl = String.format(IMG_BASE_URL, imgId);
        Target target;
        mPicasso.load(imageUrl).into(target = new Target() {
            @Override
            public void onBitmapLoaded(Bitmap bitmap, Picasso.LoadedFrom from) {
                notifyOnBitmapLoaded(index, bitmap, from);
            }

            @Override
            public void onBitmapFailed(Exception e, Drawable errorDrawable) {
                notifyOnBitmapFailed(index, e, errorDrawable);
            }

            @Override
            public void onPrepareLoad(Drawable placeHolderDrawable) {
                notifyOnPrepareLoad(index, placeHolderDrawable);
            }
        });

        // Save target so it doesn't get garbage collected.
        mTargets.add(target);

    }


    private void notifyOnBitmapLoaded(int count, Bitmap bitmap, Picasso.LoadedFrom from) {
        for (YoutubeImageInterface listener : mListeners) {
            listener.onYoutubeBitmapLoaded(count, bitmap, from);
        }
    }

    private void notifyOnBitmapFailed(int count, Exception e, Drawable errorDrawable) {
        for (YoutubeImageInterface listener : mListeners) {
            listener.onYoutubeBitmapFailed(count, e, errorDrawable);
        }
    }

    private void notifyOnPrepareLoad(int count, Drawable placeHolderDrawable) {
        for (YoutubeImageInterface listener : mListeners) {
            listener.onYoutubePrepareLoad(count, placeHolderDrawable);
        }
    }


}
