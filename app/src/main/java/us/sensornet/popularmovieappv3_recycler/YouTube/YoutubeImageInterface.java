package us.sensornet.popularmovieappv3_recycler.YouTube;

import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;

import com.squareup.picasso.Picasso;

public interface YoutubeImageInterface {

    void onYoutubeBitmapLoaded(int index, Bitmap bitmap, Picasso.LoadedFrom from);

    void onYoutubeBitmapFailed(int index, Exception e, Drawable errorDrawable);

    void onYoutubePrepareLoad(int index, Drawable placeHolderDrawable);
}
