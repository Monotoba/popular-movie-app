package us.sensornet.popularmovieappv3_recycler.Adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutCompat;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import java.util.List;
import java.util.Objects;

import us.sensornet.popularmovieappv3_recycler.Helper.ImageHelper;
import us.sensornet.popularmovieappv3_recycler.Model.MovieModel;
import us.sensornet.popularmovieappv3_recycler.R;


public class MovieAdapter extends RecyclerView.Adapter<MovieAdapter.ViewHolder> {
    private final String TAG = getClass().getSimpleName();

    @SuppressWarnings({"FieldCanBeLocal", "unused"})
    private final Context mContext;
    private List<MovieModel> mMovies;
    private GridItemClickListener mGridItemClickListener;

    @SuppressWarnings("unused")
    public MovieAdapter(@NonNull Context context) {
        mContext = context;
    }

    public MovieAdapter(@NonNull Context context, @NonNull GridItemClickListener listener) {
        mContext = context;
        mGridItemClickListener = listener;
    }


    // Constructor
    @SuppressWarnings("unused")
    public MovieAdapter(@NonNull Context context, @NonNull List<MovieModel> movies, @NonNull GridItemClickListener listener) {
        mContext = context;
        mMovies = movies;
        mGridItemClickListener = listener;

    }

    @SuppressWarnings("unused")
    public void setListener(GridItemClickListener listener) {
        mGridItemClickListener = listener;
    }

    // Reset the list with new data
    public void setMovieData(List<MovieModel> movies) {
        mMovies = movies;
        notifyDataSetChanged(); // Not sure I need this here....
    }

    // Clear all data from the adapter
    public void clearData(){
        mMovies.clear();
        notifyDataSetChanged();
    }

    // Create new Views
    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        // Create a new view
        LinearLayoutCompat view = (LinearLayoutCompat) LayoutInflater.from(parent.getContext()).inflate(R.layout.poster_cell, parent, false);

        GridLayoutManager.LayoutParams lp = (GridLayoutManager.LayoutParams) view.getLayoutParams();
        lp.setMargins(0, 0, 0, 0);
        view.setLayoutParams(lp);
        view.setMinimumWidth(parent.getMeasuredWidth() / 2);

        return new ViewHolder(view);
    }

    // Replace the contents of a view (invoked by the layout manager)
    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        // Get element from the data-set at this position
        // replace the contents of the view with that element
        MovieModel movie;

        Objects.requireNonNull(mMovies, "mMovies not initialized in onBindViewHolder");
        movie = mMovies.get(position);

        String Url = ImageHelper.ImageWidth.w500 + "/" + (movie != null ? movie.getPosterPath() : "");

        holder.mImageView.setTag(Objects.requireNonNull(movie).getId());
        holder.mImageView.setScaleType(ImageView.ScaleType.FIT_CENTER);
        holder.mImageView.setPadding(0, 0, 0, 0);
        ImageHelper.getDrawableFromNetwork(holder.mImageView, Url);

    }

    // Return the size of the data-set (invoked by the layout manager)
    // or -1 on error
    @Override
    public int getItemCount() {
        Objects.requireNonNull(mMovies, "mMovies not initialized in getItemCount");
        return mMovies.size();

    }

    // Returns null on error
    public MovieModel getMovieById(int id) {
        Objects.requireNonNull(mMovies, "mMovies not initialized in getMovieById");
        for (MovieModel movie : mMovies) {
            if (movie.getId() == id) {
                return movie;
            }
        }

        return null;
    }

    // Returns index of the given movie with Id = id
    // or -1 on error
    @SuppressWarnings("unused")
    public int getMovieIndexById(int id) {
        int index = -1;
        try {
            for (MovieModel movie : mMovies) {
                index++;
                if (movie.getId() == id) {
                    return index;
                }
            }
        } catch (NullPointerException e) {
            Log.e(TAG, "getMovieIndexById Error: " + e.getMessage());
        }
        return index;
    }


    public interface GridItemClickListener {
        void onGridItemClick(View view);
    }

    // ViewHolder
    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        // each data item is just a string in this case
        private final ImageView mImageView;


        private ViewHolder(LinearLayoutCompat view) {
            super(view);
            mImageView = view.findViewById(R.id.iv_poster_cell);
            mImageView.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            Objects.requireNonNull(mGridItemClickListener, "mGridItemClickListener not initialized in onClick");
            mGridItemClickListener.onGridItemClick(view);

        }
    }


}
