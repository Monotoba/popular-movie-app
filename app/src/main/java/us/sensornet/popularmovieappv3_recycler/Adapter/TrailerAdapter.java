package us.sensornet.popularmovieappv3_recycler.Adapter;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.constraint.ConstraintLayout;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import us.sensornet.popularmovieappv3_recycler.Database.FavoritesDbContract;
import us.sensornet.popularmovieappv3_recycler.Helper.TrailerHelper;
import us.sensornet.popularmovieappv3_recycler.Model.MovieTrailerModel;
import us.sensornet.popularmovieappv3_recycler.R;

public class TrailerAdapter extends RecyclerView.Adapter<TrailerAdapter.ViewHolder> {
    private final String TAG = getClass().getSimpleName();

    private static final String IMG_BASE_URL = "https://i.ytimg.com/vi/%s/default.jpg";
    private String mTrailerUrl = "https://www.youtube.com/watch?v=%s";
    private Picasso mPicasso = Picasso.get();

    private Context mContext;
    private View.OnClickListener mItemClickListener;
    private List<MovieTrailerModel> mTrailers = new ArrayList<>();

    public TrailerAdapter(Context context) {
        mContext = context;
    }

    public TrailerAdapter(Context context, List<MovieTrailerModel> trailers) {
        mTrailers = trailers;
        mContext = context;
    }

    public TrailerAdapter(Context context, List<MovieTrailerModel> trailers, View.OnClickListener listener) {
        mTrailers = trailers;
        mContext = context;
        mItemClickListener = listener;
    }


    public void addListener(View.OnClickListener listener) {
        mItemClickListener = listener;
    }



    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        // each data item is just a string in this case
        private ImageView mImageView;
        private ImageButton mPlayBtn;
        private ConstraintLayout mTrailerRowContainer;
        private TextView mTitle;


        public ViewHolder(View view) {
            super(view);
            mPlayBtn = view.findViewById(R.id.ib_trailer_play);
            //mPlayBtn.setOnClickListener(this);
            mImageView = view.findViewById(R.id.img_trailer);
            mTitle = view.findViewById(R.id.tv_trailer_title);
        }

        @Override
        public void onClick(View view) {
            Objects.requireNonNull(mItemClickListener, "mGridItemClickListener not initialized in onClick");
            mItemClickListener.onClick(view);
        }
    }


    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        ConstraintLayout view = (ConstraintLayout) LayoutInflater.from(parent.getContext()).inflate(R.layout.trailer_row, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        //Log.d(TAG, "onBindViewhelder: called");
        MovieTrailerModel trailer = mTrailers.get(position);

        TrailerHelper trailerImageHelper = new TrailerHelper();
        trailerImageHelper.getDrawableFromNetwork(holder.mImageView, trailer.key, false);
        holder.mTitle.setText(trailer.name);

        holder.mPlayBtn.setTag(trailer.key);

        holder.mPlayBtn.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                String movieId = (String) view.getTag();

                String url = String.format(mTrailerUrl, movieId);

                Intent playerIntent = new Intent(Intent.ACTION_VIEW);

                playerIntent.setData(Uri.parse(url));
                mContext.startActivity(playerIntent);
            }
        });
    }

    @Override
    public int getItemCount() {
        return mTrailers.size();
    }

    // Clear all adapter data
    public void clearData() {
        mTrailers.clear();
        notifyDataSetChanged();
    }

    // Reset the list with new data
    public void setData(List<MovieTrailerModel> trailers) {
        mTrailers = trailers;
        notifyDataSetChanged();
    }
}
