package us.sensornet.popularmovieappv3_recycler;

import android.content.ContentValues;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;

import java.util.List;
import java.util.Locale;
import java.util.Objects;

import us.sensornet.popularmovieappv3_recycler.Adapter.TrailerAdapter;
import us.sensornet.popularmovieappv3_recycler.Api.Service;
import us.sensornet.popularmovieappv3_recycler.Api.ServiceInterface;
import us.sensornet.popularmovieappv3_recycler.Database.FavoritesDbContract;
import us.sensornet.popularmovieappv3_recycler.Database.FavoritesDbHelper;
import us.sensornet.popularmovieappv3_recycler.Helper.GenreHelper;
import us.sensornet.popularmovieappv3_recycler.Helper.ImageHelper;
import us.sensornet.popularmovieappv3_recycler.Model.MovieListModel;
import us.sensornet.popularmovieappv3_recycler.Model.MovieModel;
import us.sensornet.popularmovieappv3_recycler.Model.MovieReviewModel;
import us.sensornet.popularmovieappv3_recycler.Model.MovieTrailerModel;
import us.sensornet.popularmovieappv3_recycler.utils.DateUtils;

public class DetailActivity extends AppCompatActivity implements ServiceInterface {
    @SuppressWarnings("unused")
    private final String TAG = getClass().getSimpleName();

    @SuppressWarnings("FieldCanBeLocal")
    private MovieModel mMovie;
    private List<MovieTrailerModel> mMovieTrailers;

    private Service mApiService;
    private String mApiKey = BuildConfig.theMovieDB3_Api_Key;

    private LayoutInflater mInflater;
    private RecyclerView mTrailerLayout;
    private int mTrailerCount;
    private RecyclerView.Adapter mTrailerAdapter;
    private RecyclerView.LayoutManager mTrailerLayoutManager;

    private String mTrailerUrl = "https://www.youtube.com/watch?v=%s";

    private List<MovieReviewModel> mReviews;

    // Database
    public FavoritesDbHelper dbHelper;
    public SQLiteDatabase mDb;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail);
        Service movieService = new Service(this, false);

        // Database
        dbHelper = new FavoritesDbHelper(this);

        if(null == dbHelper) {
            Log.d(TAG, "Database Helper is null");
            Log.d(TAG, "dbHelper is of type: " + dbHelper.getClass());
        } else {
            Log.d(TAG, "Database Helper is not null");
            Log.d(TAG, "dbHelper is of type: " + dbHelper.getClass());
        }

        mDb = this.dbHelper.getWritableDatabase();


        // Get the layout and inflater
        // Get an inflater from movie trailer list
        mTrailerLayout = findViewById(R.id.rv_movie_trailer_list);
        mInflater = LayoutInflater.from(this);
        mTrailerCount = 0;

        Gson gson = new Gson();
        String strJson = getIntent().getStringExtra("jsonMovie");
        mMovie = gson.fromJson(strJson, MovieModel.class);
        Objects.requireNonNull(mMovie, "mMovie not initialized in onCreate");

        // Get Movie Trailers
        mApiService = new Service(this, false);
        // Add this activity as listener
        mApiService.addListener(this);
        // Call the apiService
        mApiService.movieTtrailerRequest(String.valueOf(mMovie.getId()));

        // Get Movie Reviews
        mApiService.movieReviewsRequest(String.valueOf(mMovie.getId()));

        showMovieDetails(mMovie);

    }


    // Init the recycler view used for trailers
    private void initTrailerView(){
        //Log.d(TAG, "iniTrailerView: called");

        RecyclerView trailerView = findViewById(R.id.rv_movie_trailer_list);
        RecyclerView.Adapter mAdapter = new TrailerAdapter(this, mMovieTrailers);
        trailerView.setAdapter(mAdapter);
        trailerView.setLayoutManager(new LinearLayoutManager(this));
    }




    // Movie Details
    private void showMovieDetails(@NonNull MovieModel movie) {

        ImageView imageView = findViewById(R.id.img_poster);
        TextView tvTitle = findViewById(R.id.tv_title);
        TextView tvReleaseDate = findViewById(R.id.tv_release_date);
        TextView tvAverageVotes = findViewById(R.id.tv_average_vote);
        TextView tvSynopsis = findViewById(R.id.tv_synopsis);
        TextView tvGenres = findViewById(R.id.tv_genre);

        String Url = ImageHelper.ImageWidth.w500 + "/" + movie.getPosterPath();
        imageView.setTag(movie.getId());
        ImageHelper.getDrawableFromNetwork(imageView, Url);

        GenreHelper genreHelper = new GenreHelper();

        // Genres
        StringBuilder strGenres = new StringBuilder();
        List<Integer> genres = movie.getGenreIds();
        for (int id : genres) {
            strGenres.append(genreHelper.getGenreById(id).getName()).append(", ");
        }

        // Trim trailing ", " if any genres have been added
        if (strGenres.length() > 3) {
            strGenres = new StringBuilder(strGenres.substring(0, strGenres.length() - 2));
        }

        // Load the view with values
        tvTitle.setText(movie.getTitle());
        tvReleaseDate.setText(movie.getReleaseDate());
        tvAverageVotes.setText(String.format(Locale.getDefault(), "%.1f", movie.getVoteAverage()));
        tvSynopsis.setText(movie.getOverview());

        // If we have no genres there is
        // no point in displaying the header...
        if (strGenres.length() == 0) {
            TextView tvGenreLabel = findViewById(R.id.tv_genre_label);
            tvGenreLabel.setVisibility(View.INVISIBLE);
            tvGenres.setVisibility(View.INVISIBLE);
        } else {
            TextView tvGenreLabel = findViewById(R.id.tv_genre_label);
            tvGenreLabel.setVisibility(View.VISIBLE);
            tvGenres.setVisibility(View.VISIBLE);
            tvGenres.setText(strGenres.toString());
        }

        // Favorites icon
        ImageView ivFavorite = findViewById(R.id.img_favorites);
        setFavoriteIconState((false));
        ivFavorite.setTag(0);

    }


    // TODO query DB and set state appropriately
    // @android.R.drawable.btn_star_big_off
    // @android.R.drawable.btn_star_big_on
    private void setFavoriteIconState(Boolean state) {
        ImageView ivFavorite = findViewById(R.id.img_favorites);

        if(state) {
            ivFavorite.setImageResource(android.R.drawable.btn_star_big_on);
            ivFavorite.setTag(1);
        } else {
            ivFavorite.setImageResource(android.R.drawable.btn_star_big_off);
            ivFavorite.setTag(0);
        }
    }

    // Handle Favorite Icon action
    public void OnFavoriteClicked(View view) {

        // Update state
        if(0 < (int) view.getTag()) {
            setFavoriteIconState(false);
        } else {
            setFavoriteIconState(true);
        }

    }

    //-----------------------------------
    // Database Methods
    //-----------------------------------
    //String release_date, double average_vote, String synopsis, String genres, boolean favorite)
    private void saveMovieToDb(MovieModel movie) {
        ContentValues values = new ContentValues();
        values.put(FavoritesDbContract.Movies.COLUMN_MOVIE_ID, movie.getId());
        values.put(FavoritesDbContract.Movies.COLUMN_TITLE, movie.getTitle());
        values.put(FavoritesDbContract.Movies.COLUMN_RELEASE_DATE, movie.getReleaseDate());
        values.put(FavoritesDbContract.Movies.COLUMN_AVERAGE_VOTE, movie.getVoteAverage());
        values.put(FavoritesDbContract.Movies.COLUMN_OVERVIEW, movie.getOverview());
        values.put(FavoritesDbContract.Movies.COLUMN_GENRES, movie.getGenreIds().toString());
        values.put(FavoritesDbContract.Movies.COLUMN_LAST_UPDATED, DateUtils.getCurrentDateString());

        //dbHelper.insertMovie(dbHelper, movie.getId(), values);
    }

    //-----------------------------------
    // Interface Methods
    //-----------------------------------
    @Override
    public void OnMovieListReady(MovieListModel movieList) {
        //mMovies = movieList.getResults();
        //mMovieAdapter = new MovieAdapter(this, mMovies, this);
    }

    @Override
    public void OnMovieListDataReady(List<MovieModel> movies) {
        Log.d(TAG, "Called: OnMovieListDataReady");
    }

    @Override
    public void OnMovieListDataFailure(Throwable t) {
        Log.d(TAG, "Called: OnMovieListDataFailure");
    }

    @Override
    public void OnMovieTrailersDataReady(List<MovieTrailerModel> trailerList) {
        Log.d(TAG, "Called: OnMovieTrailerDataReady");
        if (null == trailerList) {
            String msg = getString(R.string.illegal_argument_exception);
            throw new IllegalArgumentException(msg);
        }
        mMovieTrailers = trailerList;
        initTrailerView();
    }

    @Override
    public void OnMovieTrailersDataFailure(Throwable t) {
        Log.d(TAG, "Called: OnMovieTrailerDataFailure");
        Toast.makeText(this, t.getMessage(), Toast.LENGTH_SHORT).show();
    }

    @Override
    public void OnMovieReviewDataReady(List<MovieReviewModel> reviewList) {
        //insertReviewRows(reviewList);
        // Get the linear layout used for the list
        LinearLayout reviewLayout = findViewById(R.id.ll_movie_review_list);

        // Add all reviews to the list
        for (MovieReviewModel review : reviewList) {

            // Inflate a row view object
            // Make sure root is null or this will overwrite the same object on each iteration!
            View view = mInflater.inflate(R.layout.review_row, null, false);

            TextView tvAuthor = view.findViewById(R.id.tv_review_author);
            tvAuthor.setText(review.author);

            TextView tvBody = view.findViewById(R.id.tv_review_body);
            tvBody.setText(review.content);

            // Now add the row to the linear layout
            reviewLayout.addView(view);//, mTrailerCount++);
        }

    }

    @Override
    public void OnMovieReviewDataFailure(Throwable t) {
        Log.d(TAG, "Movie Review loading failed: " + t.getMessage());
    }


}
