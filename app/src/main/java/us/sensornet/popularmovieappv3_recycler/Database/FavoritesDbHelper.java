package us.sensornet.popularmovieappv3_recycler.Database;

import android.content.ContentValues;
import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

public class FavoritesDbHelper  extends SQLiteOpenHelper {
    String TAG = getClass().getSimpleName();

    public Context mContext;

    // If you change the database schema you must increment the version.
    public static final int DATABASE_VERSION = 1;
    public static final String DATABASE_NAME = FavoritesDbContract.DATABASE_NAME;

    public FavoritesDbHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
        Log.d(TAG, "Constructor called");
    }

    // Called to create table if not exists
    @Override
    public void onCreate(SQLiteDatabase db) {
        Log.d(TAG, "FavoritesDbHelper: onCreate() called");
        try {
            db.execSQL(FavoritesDbContract.SQL_CREATE_MOVIES_TABLE);
            db.execSQL(FavoritesDbContract.SQL_CREATE_REVIEWS_TABLE);
            db.execSQL(FavoritesDbContract.SQL_CREATE_TRAILERS_TABLE);
            db.execSQL(FavoritesDbContract.SQL_CREATE_POSTERS_TABLE);
        } catch(Exception e) {
            Log.d(TAG, "Database Error: " + e.getMessage());
        }
    }

    // Called to upgrade database if necessary
    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }

    public void onDowngrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        //onUpgrade(db, oldVersion, newVersion);
    }

}
