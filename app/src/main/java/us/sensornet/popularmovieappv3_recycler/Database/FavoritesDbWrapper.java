package us.sensornet.popularmovieappv3_recycler.Database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Movie;

import java.util.List;

public class FavoritesDbWrapper {
    private static final String TAG = "FavoritesDbWrapper";

    Context mContext;

    Cursor result = null;

    private SQLiteDatabase mDb;

    private FavoritesDbHelper mDbHelper;

    private Boolean mWritable;

    public FavoritesDbWrapper(Context context, Boolean writable) {
        mContext = context;
        mDbHelper = new FavoritesDbHelper(mContext);
        mWritable = writable;
        if(mWritable) {
            mDb = mDbHelper.getWritableDatabase();
        } else {
            mDb = mDbHelper.getReadableDatabase();
        }
    }



    //--------------------------------------------
    // Movies
    //--------------------------------------------

    // Insert Movie into database using content values
    public long insertMovie(ContentValues values) {

        return mDb.insert(FavoritesDbContract.Movies.TABLE_NAME, null, values);
    }


    // Insert Movie into database using individual fields
    public long insertMovie(int movie_id, String title, String release_date,
                            double average_vote, String overview, String genres, boolean favorite) {
        if(!mWritable) return 0;

        ContentValues values = new ContentValues();
        values.put(FavoritesDbContract.Movies.COLUMN_MOVIE_ID, String.valueOf(movie_id));
        values.put(FavoritesDbContract.Movies.COLUMN_TITLE, title);
        values.put(FavoritesDbContract.Movies.COLUMN_RELEASE_DATE, release_date);
        values.put(FavoritesDbContract.Movies.COLUMN_AVERAGE_VOTE, String.valueOf(average_vote));
        values.put(FavoritesDbContract.Movies.COLUMN_OVERVIEW, overview);
        values.put(FavoritesDbContract.Movies.COLUMN_GENRES, genres);
        values.put(FavoritesDbContract.Movies.COLUMN_IS_FAVORITE, String.valueOf(favorite));

        return mDb.insert(FavoritesDbContract.Movies.TABLE_NAME, null, values);
    }


    // Update movie in database using content values
    public int updateMovie(ContentValues values) {

        String[] where_args = new String[1];
        where_args[0] = values.getAsString("id");

        return mDb.update(FavoritesDbContract.Movies.TABLE_NAME, values, "id=?",where_args);
    }


    // Update movie in database using individual fields
    public int updateMovie(SQLiteDatabase db, int movie_id, String title, String release_date,
                            double average_vote, String overview, String genres,
                            boolean favorite, String where_clause, String[] where_args) {

        ContentValues values = new ContentValues();
        values.put(FavoritesDbContract.Movies.COLUMN_MOVIE_ID, String.valueOf(movie_id));
        values.put(FavoritesDbContract.Movies.COLUMN_TITLE, title);
        values.put(FavoritesDbContract.Movies.COLUMN_RELEASE_DATE, release_date);
        values.put(FavoritesDbContract.Movies.COLUMN_AVERAGE_VOTE, String.valueOf(average_vote));
        values.put(FavoritesDbContract.Movies.COLUMN_OVERVIEW, overview);
        values.put(FavoritesDbContract.Movies.COLUMN_GENRES, genres);

        return mDb.update(FavoritesDbContract.Movies.TABLE_NAME, values, where_clause, where_args);
    }

    // Delete movie using Content Value.
    // Note: Must include id of record to delete
    public int removeMovie(ContentValues values) {

        String[] where_args = new String[1];
        where_args[0] = values.getAsString("id");

        return mDb.delete(FavoritesDbContract.Movies.TABLE_NAME, "id=?", where_args);

    }

    // Delete Movie using int id
    public int removeMovie(int movie_id) {

        String[] where_args = new String[1];
        where_args[0] = String.valueOf(movie_id);

        return mDb.delete(FavoritesDbContract.Movies.TABLE_NAME, "id=?", where_args);

    }

    // Delete Movie using String id
    public int removeMovie(String movie_id) {

        String[] where_args = new String[1];
        where_args[0] = movie_id;

        return mDb.delete(FavoritesDbContract.Movies.TABLE_NAME, "id=?", where_args);

    }

    // Query for movies
    public Cursor getMovies(String[] projection, String selection, String[] selection_args, String sort_order) {
        return mDb.query(FavoritesDbContract.Movies.TABLE_NAME, projection, selection, selection_args, null, null, sort_order);
    }



    //-------------------------------------------------
    // Reviews
    //-------------------------------------------------
    // Insert Reviews into database using ContentValues
    // Values must contain the movie_id of the parent movie
    public long insertReview( ContentValues values) {

        return mDb.insert(FavoritesDbContract.Reviews.TABLE_NAME, null, values);

    }


    // insert review using fields
    public long insertReview(int movie_id, String author, String comment) {

        ContentValues values = new ContentValues();
        values.put(FavoritesDbContract.Reviews.COLUMN_MOVIE_ID, String.valueOf(movie_id));
        values.put(FavoritesDbContract.Reviews.COLUMN_AUTHOR, author);
        values.put(FavoritesDbContract.Reviews.COLUMN_COMMENT, comment);

        return mDb.insert(FavoritesDbContract.Reviews.TABLE_NAME, null, values);
    }

    public void updateReview(int movie_id, String author, String comment, String where_clause, String[] where_args) {

        ContentValues values = new ContentValues();
        values.put(FavoritesDbContract.Reviews.COLUMN_MOVIE_ID, String.valueOf(movie_id));
        values.put(FavoritesDbContract.Reviews.COLUMN_AUTHOR, author);
        values.put(FavoritesDbContract.Reviews.COLUMN_COMMENT, comment);

        mDb.update(FavoritesDbContract.Reviews.TABLE_NAME, values, where_clause, where_args);
    }

    public void removeReview(int movie_id) {

        String[] where_args = new String[1];
        where_args[0] = String.valueOf(movie_id);

        mDb.delete(FavoritesDbContract.Reviews.TABLE_NAME, "id=?", where_args);

    }

    // Query for reviews
    public Cursor getReviews(String[] projection, String[] selection, String where_clause, String[] where_args, String sort_order) {
        return mDb.query(FavoritesDbContract.Reviews.TABLE_NAME, projection, where_clause, where_args, null, null, sort_order);
    }



    //-----------------------------------------------------
    // Trailers
    //-----------------------------------------------------
    public long insertTrailer(int movie_id, ContentValues values) {

        return mDb.insert(FavoritesDbContract.Trailers.TABLE_NAME, null, values);

    }

    public long insertTrailer(int movie_id, String placeholder_url, String video_url){

        ContentValues values = new ContentValues();
        values.put(FavoritesDbContract.Trailers.COLUMN_MOVIE_ID, String.valueOf(movie_id));
        values.put(FavoritesDbContract.Trailers.COLUMN_PLACEHOLDER_URL, placeholder_url);
        values.put(FavoritesDbContract.Trailers.COLUMN_VIDEO_URL, video_url);

        return mDb.insert(FavoritesDbContract.Trailers.TABLE_NAME, null, values);
    }

    public void updateTrailer(int movie_id, String placeholder_url, String video_url, String where_clause, String[] where_args){

        ContentValues values = new ContentValues();
        values.put(FavoritesDbContract.Trailers.COLUMN_MOVIE_ID, String.valueOf(movie_id));
        values.put(FavoritesDbContract.Trailers.COLUMN_PLACEHOLDER_URL, placeholder_url);
        values.put(FavoritesDbContract.Trailers.COLUMN_VIDEO_URL, video_url);

        mDb.update(FavoritesDbContract.Trailers.TABLE_NAME, values, where_clause, where_args);
    }

    public void removeTrailer(int trailer_id) {

        String[] where_args = new String[1];
        where_args[0] = String.valueOf(trailer_id);

        mDb.delete(FavoritesDbContract.Trailers.TABLE_NAME, "id=?",where_args);
    }

    // Query for movies
    public Cursor getTrailer(String[] projection, String[] selection, String where_clause, String[] where_args, String sort_order) {
        return mDb.query(FavoritesDbContract.Trailers.TABLE_NAME, projection, where_clause, where_args, null, null, sort_order);
    }



    //-----------------------------------------
    // Movie Posters
    //-----------------------------------------
    public void insertPoster(int movie_id, ContentValues values) {

        mDb.insert(FavoritesDbContract.Posters.TABLE_NAME, null, values);

    }

    public void insertPoster(int movie_id, String poster_url) {

        ContentValues values = new ContentValues();
        values.put(FavoritesDbContract.Posters.COLUMN_MOVIE_ID, String.valueOf(movie_id));
        values.put(FavoritesDbContract.Posters.COLUMN_POSTER_URL, poster_url);

        mDb.insert(FavoritesDbContract.Posters.TABLE_NAME, null, values);
    }

    public void updatePoster(int movie_id, String poster_url, String where_clause, String[] where_args) {

        ContentValues values = new ContentValues();
        values.put(FavoritesDbContract.Posters.COLUMN_MOVIE_ID, String.valueOf(movie_id));
        values.put(FavoritesDbContract.Posters.COLUMN_POSTER_URL, poster_url);

        mDb.update(FavoritesDbContract.Posters.TABLE_NAME, values, where_clause, where_args);
    }

    public void removePoster(int poster_id) {

        String[] where_args = new String[1];
        where_args[0] = String.valueOf(poster_id);

        mDb.delete(FavoritesDbContract.Posters.TABLE_NAME, "id=?",where_args);
    }

    // Query for Posters
    public Cursor getPosters(String[] projection, String[] selection, String where_clause, String[] where_args, String sort_order) {
        return mDb.query(FavoritesDbContract.Posters.TABLE_NAME, projection, where_clause, where_args, null, null, sort_order);
    }

}
