package us.sensornet.popularmovieappv3_recycler.Database;

import android.net.Uri;
import android.provider.BaseColumns;

//import us.sensornet.popularmovieappv3_recycler.Model.MovieModel;

public class FavoritesDbContract {

    public static final String DATABASE_NAME = "popular_movies";

    public static final String AUTHORITY = "us.sensornet.popularmovieappv3_recycler";
    public static final Uri BASE_CONTENT_URI = Uri.parse("content://" + AUTHORITY);
    public static final String FAVORITES_PATH = "favorites";
    public static final Uri FAVORITES_CONTENT_URI = BASE_CONTENT_URI.buildUpon().appendPath(FAVORITES_PATH).build();


    private FavoritesDbContract() {}

    public static class Movies implements BaseColumns {

        public static final String TABLE_NAME = "favorites";

        public static final String COLUMN_ID = "id";
        public static final String COLUMN_MOVIE_ID = "movie_id";
        public static final String COLUMN_TITLE = "title";
        public static final String COLUMN_RELEASE_DATE = "release_date";
        public static final String COLUMN_GENRES = "genres";
        public static final String COLUMN_ADULT = "adult";
        public static final String COLUMN_OVERVIEW = "overview";
        public static final String COLUMN_VOTE_COUNT = "vote_count";
        public static final String COLUMN_AVERAGE_VOTE = "average_vote";
        public static final String COLUMN_POPULARITY = "popularity";
        public static final String COLUMN_POSTER_PATH = "poster_path";
        public static final String COLUMN_VIDEO = "video";
        public static final String COLUMN_ORIGINAL_LANGUAGE = "original_language";
        public static final String COLUMN_ORIGINAL_TITLE = "original_title";
        public static final String COLUMN_BACKDROP_PATH = "backdrop_path";
        public static final String COLUMN_LAST_UPDATED = "last_updated";
        public static final String COLUMN_IS_FAVORITE = "is_favorite";

    }


    /* Movies */
    public static final String SQL_CREATE_MOVIES_TABLE =
            "CREATE TABLE " + Movies.TABLE_NAME + " (" +
                    Movies.COLUMN_ID + " INTEGER PRIMARY_KEY" +
                    Movies.COLUMN_MOVIE_ID + " INTEGER, " +
                    Movies.COLUMN_TITLE + " TEXT, " +
                    Movies. COLUMN_RELEASE_DATE + "TEXT, " +
                    Movies.COLUMN_GENRES + " TEXT, " +
                    Movies.COLUMN_ADULT + " INTTEGER DEFAULT 0, " +
                    Movies.COLUMN_OVERVIEW + " TEXT, " +
                    Movies.COLUMN_VOTE_COUNT + " NUMBER, " +
                    Movies.COLUMN_AVERAGE_VOTE + " NUMBER, " +
                    Movies.COLUMN_POPULARITY + " NUMBER, " +
                    Movies.COLUMN_POSTER_PATH + " TEXT, " +
                    Movies.COLUMN_VIDEO + " INTEGER, " +
                    Movies.COLUMN_ORIGINAL_LANGUAGE + " TEXT, " +
                    Movies.COLUMN_ORIGINAL_TITLE + " TEXT, " +
                    Movies.COLUMN_BACKDROP_PATH + " TEXT, " +
                    Movies.COLUMN_LAST_UPDATED + " TEXT, " +
                    Movies.COLUMN_IS_FAVORITE + " INTEGER DEFAULT 0, " + //No Bool type in SQLite so use 0 = false, 1 = true
                    "UNIQUE( " + Movies.COLUMN_MOVIE_ID + "));";


    public static final String SQL_DELETE_MOVIES =
            "DROP TABLE IF EXISTS " + Movies.TABLE_NAME;


    /* Favorites */
    public static class Favorites implements BaseColumns {
        public static final String TABLE_NAME = "favorites";

        public static final String COLUMN_ID = "id";
        public static final String COLUMN_MOVIE_ID = "movie_id";
        public static final String COLUMN_TITLE = "title";
        public static final String COLUMN_RELEASE_DATE = "release_date";
        public static final String COLUMN_GENRES = "genres";
        public static final String COLUMN_ADULT = "adult";
        public static final String COLUMN_OVERVIEW = "overview";
        public static final String COLUMN_VOTE_COUNT = "vote_count";
        public static final String COLUMN_AVERAGE_VOTE = "average_vote";
        public static final String COLUMN_POPULARITY = "popularity";
        public static final String COLUMN_POSTER_PATH = "poster_path";
        public static final String COLUMN_VIDEO = "video";
        public static final String COLUMN_ORIGINAL_LANGUAGE = "original_language";
        public static final String COLUMN_ORIGINAL_TITLE = "original_title";
        public static final String COLUMN_BACKDROP_PATH = "backdrop_path";
        public static final String COLUMN_LAST_UPDATED = "last_updated";
        public static final String COLUMN_IS_FAVORITE = "is_favorite";
    }

    public static final String SQL_CREATE_FAVORITES_TABLE =
            "CREATE TABLE " + Favorites.TABLE_NAME + " (" +
                    Favorites.COLUMN_ID + " INTEGER PRIMARY_KEY" +
                    Favorites.COLUMN_MOVIE_ID + " INTEGER, " +
                    Favorites.COLUMN_TITLE + " TEXT, " +
                    Favorites. COLUMN_RELEASE_DATE + "TEXT, " +
                    Favorites.COLUMN_GENRES + " TEXT, " +
                    Favorites.COLUMN_ADULT + " INTTEGER DEFAULT 0, " +
                    Favorites.COLUMN_OVERVIEW + " TEXT, " +
                    Favorites.COLUMN_VOTE_COUNT + " NUMBER, " +
                    Favorites.COLUMN_AVERAGE_VOTE + " NUMBER, " +
                    Favorites.COLUMN_POPULARITY + " NUMBER, " +
                    Favorites.COLUMN_POSTER_PATH + " TEXT, " +
                    Favorites.COLUMN_VIDEO + " INTEGER, " +
                    Favorites.COLUMN_ORIGINAL_LANGUAGE + " TEXT, " +
                    Favorites.COLUMN_ORIGINAL_TITLE + " TEXT, " +
                    Favorites.COLUMN_BACKDROP_PATH + " TEXT, " +
                    Favorites.COLUMN_LAST_UPDATED + " TEXT, " +
                    Favorites.COLUMN_IS_FAVORITE + " INTEGER DEFAULT 1, " + //No Bool type in SQLite so use 0 = false, 1 = true
                    "UNIQUE( " + Favorites.COLUMN_MOVIE_ID + "));";


    public static final String SQL_DELETE_FAVORITES =
            "DROP TABLE IF EXISTS " + Favorites.TABLE_NAME;



    /* Reviews */
    public static class Reviews implements BaseColumns {
        public static final String TABLE_NAME = "reviews";

        public static final String COLUMN_ID = "id";
        public static final String COLUMN_MOVIE_ID = "movie_id";
        public static final String COLUMN_AUTHOR = "author";
        public static final String COLUMN_COMMENT = "comment";
    }

    public static final String SQL_CREATE_REVIEWS_TABLE =
            "CREATE TABLE " + Reviews.TABLE_NAME + " (" +
                    Reviews.COLUMN_ID + " INTEGER PRIMARY KEY, " +
                    Reviews.COLUMN_MOVIE_ID + " INTEGER, " +
                    Reviews.COLUMN_AUTHOR + " TEXT, " +
                    Reviews.COLUMN_COMMENT + " TEXT, " +
                    "UNIQUE( " + Reviews.COLUMN_MOVIE_ID + "));";

    public static final String SQL_DELETE_REVIEWS =
            "DROP TABLE IF EXISTS " + Reviews.TABLE_NAME;



    /* Trailers */
    public static class Trailers implements BaseColumns {
        public static final String TABLE_NAME = "trailers";

        public static final String COLUMN_ID = "id";
        public static final String COLUMN_MOVIE_ID = "movie_id";
        public static final String COLUMN_PLACEHOLDER_URL = "placeholder_url";
        public static final String COLUMN_VIDEO_URL = "video_url";
    }

    public static final String SQL_CREATE_TRAILERS_TABLE =
            "CREATE TABLE " + Trailers.TABLE_NAME + " ( " +
                    Trailers.COLUMN_ID + " INTEGER PRIMARY KEY, " +
                    Trailers.COLUMN_MOVIE_ID + " INTEGER, " +
                    Trailers.COLUMN_PLACEHOLDER_URL + " TEXT, " +
                    Trailers.COLUMN_VIDEO_URL + " TEXT, " +
                    "UNIQUE( " + Trailers.COLUMN_MOVIE_ID + "));";

    public static final String SQL_DELETE_TRAILERS =
            "DROP TABLE IF EXISTS " + Trailers.TABLE_NAME;



    /* Posters */
    public static class Posters implements BaseColumns {
        public static final String TABLE_NAME = "posters";

        public static final String COLUMN_ID = "id";
        public static final String COLUMN_MOVIE_ID = "movie_id";
        public static final String COLUMN_POSTER_URL = "poster_url";
    }

    public static final String SQL_CREATE_POSTERS_TABLE =
            "CREATE TABLE " + Posters.TABLE_NAME + " ( " +
                    Posters.COLUMN_ID + " INTEGER PRIMARY KEY, " +
                    Posters.COLUMN_MOVIE_ID + " TEXT, " +
                    Posters.COLUMN_POSTER_URL + " TEXT, " +
                    "UNIQUE( " + Posters.COLUMN_MOVIE_ID + "));";

    public static final String SQL_DELETE_POSTERS =
            "DROP TABLE IF EXISTS " + Posters.TABLE_NAME;

}
