package us.sensornet.popularmovieappv3_recycler.Model;

import org.apache.commons.lang3.builder.ToStringBuilder;

@SuppressWarnings("ALL")
public class GenreModel {

    public Integer mId;
    public String mName;

    public GenreModel() {
    }

    public GenreModel(int id, String name) {
        mId = id;
        mName = name;
    }

    public Integer getId() {
        return mId;
    }

    public void setId(Integer id) {
        this.mId = id;
    }

    public String getName() {
        return mName;
    }

    public void setName(String name) {
        this.mName = name;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this).append("id", mId).append("name", mName).toString();
    }

}