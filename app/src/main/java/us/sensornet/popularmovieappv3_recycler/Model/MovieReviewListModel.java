package us.sensornet.popularmovieappv3_recycler.Model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import org.apache.commons.lang3.builder.ToStringBuilder;

import java.util.ArrayList;
import java.util.List;

public class MovieReviewListModel implements Parcelable {

    public final static Parcelable.Creator<MovieReviewListModel> CREATOR = new Creator<MovieReviewListModel>() {


        @SuppressWarnings({"unchecked"})
        public MovieReviewListModel createFromParcel(Parcel in) {
            return new MovieReviewListModel(in);
        }

        public MovieReviewListModel[] newArray(int size) {
            return (new MovieReviewListModel[size]);
        }

    };
    @SerializedName("id")
    @Expose
    public Integer id;
    @SerializedName("page")
    @Expose
    public Integer page;
    @SerializedName("results")
    @Expose
    public List<MovieReviewModel> results = new ArrayList<MovieReviewModel>();
    @SerializedName("total_pages")
    @Expose
    public Integer totalPages;
    @SerializedName("total_MovieReviewModels")
    @Expose
    public Integer totalMovieReviewModels;

    protected MovieReviewListModel(Parcel in) {
        this.id = ((Integer) in.readValue((Integer.class.getClassLoader())));
        this.page = ((Integer) in.readValue((Integer.class.getClassLoader())));
        in.readList(this.results, (us.sensornet.popularmovieappv3_recycler.Model.MovieReviewModel.class.getClassLoader()));
        this.totalPages = ((Integer) in.readValue((Integer.class.getClassLoader())));
        this.totalMovieReviewModels = ((Integer) in.readValue((Integer.class.getClassLoader())));
    }

    /**
     * No args constructor for use in serialization
     */
    public MovieReviewListModel() {
    }

    /**
     * @param id
     * @param results
     * @param totalMovieReviewModels
     * @param page
     * @param totalPages
     */
    public MovieReviewListModel(Integer id, Integer page, List<MovieReviewModel> results, Integer totalPages, Integer totalMovieReviewModels) {
        super();
        this.id = id;
        this.page = page;
        this.results = results;
        this.totalPages = totalPages;
        this.totalMovieReviewModels = totalMovieReviewModels;
    }

    public List<MovieReviewModel> getResults() {
        return results;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this).append("id", id).append("page", page).append("MovieReviewModels", results).append("totalPages", totalPages).append("totalMovieReviewModels", totalMovieReviewModels).toString();
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(id);
        dest.writeValue(page);
        dest.writeList(results);
        dest.writeValue(totalPages);
        dest.writeValue(totalMovieReviewModels);
    }

    public int describeContents() {
        return 0;
    }

}