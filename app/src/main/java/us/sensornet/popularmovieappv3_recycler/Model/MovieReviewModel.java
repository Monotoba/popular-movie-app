package us.sensornet.popularmovieappv3_recycler.Model;


import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import org.apache.commons.lang3.builder.ToStringBuilder;

public class MovieReviewModel implements Parcelable {

    public final static Parcelable.Creator<MovieReviewModel> CREATOR = new Creator<MovieReviewModel>() {


        @SuppressWarnings({"unchecked"})
        public MovieReviewModel createFromParcel(Parcel in) {
            return new MovieReviewModel(in);
        }

        public MovieReviewModel[] newArray(int size) {
            return (new MovieReviewModel[size]);
        }

    };
    @SerializedName("author")
    @Expose
    public String author;
    @SerializedName("content")
    @Expose
    public String content;
    @SerializedName("id")
    @Expose
    public String id;
    @SerializedName("url")
    @Expose
    public String url;

    protected MovieReviewModel(Parcel in) {
        this.author = ((String) in.readValue((String.class.getClassLoader())));
        this.content = ((String) in.readValue((String.class.getClassLoader())));
        this.id = ((String) in.readValue((String.class.getClassLoader())));
        this.url = ((String) in.readValue((String.class.getClassLoader())));
    }

    /**
     * No args constructor for use in serialization
     */
    public MovieReviewModel() {
    }

    /**
     * @param id
     * @param content
     * @param author
     * @param url
     */
    public MovieReviewModel(String author, String content, String id, String url) {
        super();
        this.author = author;
        this.content = content;
        this.id = id;
        this.url = url;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this).append("author", author).append("content", content).append("id", id).append("url", url).toString();
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(author);
        dest.writeValue(content);
        dest.writeValue(id);
        dest.writeValue(url);
    }

    public int describeContents() {
        return 0;
    }

}