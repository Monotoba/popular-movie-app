package us.sensornet.popularmovieappv3_recycler.Model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import org.apache.commons.lang3.builder.ToStringBuilder;

import java.util.ArrayList;
import java.util.List;


public class MovieTrailerListModel implements Parcelable {

    public final static Parcelable.Creator<MovieTrailerListModel> CREATOR = new Creator<MovieTrailerListModel>() {


        @SuppressWarnings({"unchecked"})
        public MovieTrailerListModel createFromParcel(Parcel in) {
            return new MovieTrailerListModel(in);
        }

        public MovieTrailerListModel[] newArray(int size) {
            return (new MovieTrailerListModel[size]);
        }

    };
    @SerializedName("id")
    @Expose
    public Integer id;
    @SerializedName("results")
    @Expose
    private List<MovieTrailerModel> results = new ArrayList<MovieTrailerModel>();

    protected MovieTrailerListModel(Parcel in) {
        this.id = ((Integer) in.readValue((Integer.class.getClassLoader())));
        in.readList(this.results, (us.sensornet.popularmovieappv3_recycler.Model.MovieTrailerModel.class.getClassLoader()));
    }

    /**
     * No args constructor for use in serialization
     */
    public MovieTrailerListModel() {
    }

    /**
     * @param id
     * @param results
     */
    public MovieTrailerListModel(Integer id, List<MovieTrailerModel> results) {
        super();
        this.id = id;
        this.results = results;
    }

    public List<MovieTrailerModel> getResults() {
        return results;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this).append("id", id).append("results", results).toString();
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(id);
        dest.writeList(results);
    }

    public int describeContents() {
        return 0;
    }

}
