package us.sensornet.popularmovieappv3_recycler.utils;

import android.support.annotation.NonNull;

import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

@Deprecated
public final class DateUtils {


    public static String getCurrentDateString() {
        SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
        Date date = new Date();
        return formatter.format(date);
    }

    // Because the initialization of the date is in milliseconds
    // and the timestamp is in seconds !
    public static String getStringDateFromTimestamp(Timestamp timestamp){
        SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
        Date date = new Date(timestamp.getTime());
        return formatter.format(date);
    }

    public static Timestamp getTimestampFromDateString(@NonNull String dateString) {
        SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
        Date date = null;
        try {
            date = formatter.parse(dateString);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return new Timestamp(date.getTime());
    }

    public static long getLongFromDateString(String dateString) {
        @Deprecated
        Date date = new Date(dateString);
        return date.getTime();
    }
}
