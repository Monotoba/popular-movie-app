package us.sensornet.popularmovieappv3_recycler.Api;

import java.util.List;

import us.sensornet.popularmovieappv3_recycler.Model.MovieListModel;
import us.sensornet.popularmovieappv3_recycler.Model.MovieModel;
import us.sensornet.popularmovieappv3_recycler.Model.MovieReviewModel;
import us.sensornet.popularmovieappv3_recycler.Model.MovieTrailerModel;

public interface ServiceInterface {

    // Movie Details
    @SuppressWarnings({"EmptyMethod", "unused"})
    void OnMovieListReady(MovieListModel movieList);

    @SuppressWarnings({"EmptyMethod", "unused"})
    void OnMovieListDataReady(List<MovieModel> movies);

    @SuppressWarnings({"EmptyMethod", "unused"})
    void OnMovieListDataFailure(Throwable t);

    // Movie Trailers
    @SuppressWarnings({"EmptyMethod", "unused"})
    void OnMovieTrailersDataReady(List<MovieTrailerModel> trailerList);

    @SuppressWarnings({"EmptyMethod", "unused"})
    void OnMovieTrailersDataFailure(Throwable t);

    // Movie Reviews
    @SuppressWarnings({"EmptyMethod", "unused"})
    void OnMovieReviewDataReady(List<MovieReviewModel> trailerList);

    @SuppressWarnings({"EmptyMethod", "unused"})
    void OnMovieReviewDataFailure(Throwable t);
}
