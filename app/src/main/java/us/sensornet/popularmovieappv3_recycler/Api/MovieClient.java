package us.sensornet.popularmovieappv3_recycler.Api;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;
import retrofit2.http.Query;
import us.sensornet.popularmovieappv3_recycler.Model.MovieListModel;
import us.sensornet.popularmovieappv3_recycler.Model.MovieModel;
import us.sensornet.popularmovieappv3_recycler.Model.MovieReviewListModel;
import us.sensornet.popularmovieappv3_recycler.Model.MovieTrailerListModel;

@SuppressWarnings({"WeakerAccess", "unused"})
public interface MovieClient {

    @GET("/3/movie/{type}")
    Call<MovieListModel> getMovies(@Path("type") String type, @Query("api_key") String api_key);

    @GET("/3/movie/{movieId}")
    Call<MovieModel> getMovieDetails(@Path("movieId") String movieId);

    @GET("/3/movie/{movieId}/videos")
    Call<MovieTrailerListModel> getMovieTrailers(@Path("movieId") String movieId, @Query("api_key") String api_key);

    @GET("/3/movie/{movieId}/reviews")
    Call<MovieReviewListModel> getMovieReviews(@Path("movieId") String movieId, @Query("api_key") String api_key);
}