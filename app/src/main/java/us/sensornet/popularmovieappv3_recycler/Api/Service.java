package us.sensornet.popularmovieappv3_recycler.Api;

import android.content.Context;
import android.util.Log;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import us.sensornet.popularmovieappv3_recycler.BuildConfig;
import us.sensornet.popularmovieappv3_recycler.Model.MovieListModel;
import us.sensornet.popularmovieappv3_recycler.Model.MovieModel;
import us.sensornet.popularmovieappv3_recycler.Model.MovieReviewListModel;
import us.sensornet.popularmovieappv3_recycler.Model.MovieReviewModel;
import us.sensornet.popularmovieappv3_recycler.Model.MovieTrailerListModel;
import us.sensornet.popularmovieappv3_recycler.Model.MovieTrailerModel;


@SuppressWarnings("SpellCheckingInspection")
public class Service {
    private final String TAG = getClass().getSimpleName();


    private static OkHttpClient mClient;
    //private static Request mRequest;
    private static GsonConverterFactory mGsonFactory;

    @SuppressWarnings({"unused", "FieldCanBeLocal"})
    private final Context mContext;
    private final String API_BASE_URL = "https://api.themoviedb.org";
    private final String API_KEY = BuildConfig.theMovieDB3_Api_Key;
    private final List<ServiceInterface> mListeners;

    @SuppressWarnings({"FieldCanBeLocal", "unused"})
    private Boolean mIsLogging = false;

    public Service(Context context, Boolean islogging) {
        mContext = context;
        mIsLogging = islogging;
        mListeners = new ArrayList<>();

        if (null == mClient) {
            mClient = new OkHttpClient();

        }

        if (mIsLogging) {
            HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
            interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
            mClient = new OkHttpClient.Builder().addInterceptor(interceptor).readTimeout(3000, TimeUnit.MILLISECONDS).build();

        }



        // Create a single instance of GsonConverterFactory
        if (null == mGsonFactory) {
            mGsonFactory = GsonConverterFactory.create();
        }
    }


    public void popularMovieRequest(String mode) {
        Retrofit retrofit = new Retrofit.Builder().baseUrl(API_BASE_URL).client(mClient).addConverterFactory(mGsonFactory).build();

        MovieClient moviesClient = retrofit.create(MovieClient.class);


        Call<MovieListModel> call = moviesClient.getMovies(mode, API_KEY);


        // Use call object to make request
        call.enqueue(new Callback<MovieListModel>() {

            @SuppressWarnings("NullableProblems")
            @Override
            public void onResponse(Call<MovieListModel> call, Response<MovieListModel> response) {

                if (response.isSuccessful()) {
                    Log.d(TAG, "---- Results: " + response.body());
                    MovieListModel movies = response.body();

                    // Pass back only the MovieList
                    try {
                        //noinspection ConstantConditions
                        Log.d(TAG, "---- Data Ready.....");
                        notifyMovieListDataReady(movies.getResults());
                    } catch (NullPointerException e) {
                        Log.d(TAG, "Network query results cannot be parse into MovieListModel Object");
                        Log.d(TAG, "Error: " + e.getMessage());
                    }

                } else {
                    Log.d(TAG, "Retrofit Error: " + response.message() + " " + response.errorBody());
                }
            }

            @SuppressWarnings("NullableProblems")
            @Override
            public void onFailure(Call<MovieListModel> call, Throwable t) {
                Log.d(TAG, "Error: Retrofit call unseucessful....");
                notifyMovieListFailure(t);
            }
        });
    }

    public void addListener(ServiceInterface listener) {
        mListeners.add(listener);
    }

    @SuppressWarnings("unused")
    public void movieDetailsRequest(String movieId) {
        Retrofit retrofit = new Retrofit.Builder().baseUrl(API_BASE_URL).client(mClient).addConverterFactory(mGsonFactory).build();

        MovieClient moviesClient = retrofit.create(MovieClient.class);

        Call<MovieListModel> call = moviesClient.getMovies("", API_KEY);

        // Use call object to make request
        call.enqueue(new Callback<MovieListModel>() {

            @SuppressWarnings("NullableProblems")
            @Override
            public void onResponse(Call<MovieListModel> call, Response<MovieListModel> response) {

                if (response.isSuccessful()) {
                    Log.d(TAG, "Retrofit Error: " + response.message() + " " + response.errorBody());
                    MovieListModel movies = response.body();
                    Log.d(TAG, "Response: " + movies.toString());

                    // Pass back only the MovieList
                    try {
                        //noinspection ConstantConditions
                        notifyMovieListDataReady(movies.getResults());
                    } catch (NullPointerException e) {
                        Log.d(TAG, "Network query results cannot be parse into MovieListModel Object");
                        Log.d(TAG, "Error: " + e.getMessage());
                    }

                } else {
                    Log.d(TAG, "Retrofit Error: " + response.message() + " " + response.errorBody());
                }
            }

            @SuppressWarnings("NullableProblems")
            @Override
            public void onFailure(Call<MovieListModel> call, Throwable t) {
                Log.d(TAG, "Error: Retrofit call unsucessful....");
                notifyMovieListFailure(t);
            }
        });

    }

    public static class QueryModes {
        public static final String MostPopular = "popular";
        public static final String TopRated = "top_rated";
        public static final String Favorites = "favorites";
    }


    private void notifyMovieListFailure(Throwable t) {
        for (ServiceInterface listener : mListeners) {
            Log.d(TAG, " ---- NotifyMovieFailed ----");
            listener.OnMovieListDataFailure(t);
        }
    }


    private void notifyMovieListDataReady(List<MovieModel> movies) {
        for (ServiceInterface listener : mListeners) {
            Log.d(TAG, "---- NotifyMovieListDataReady ----");
            listener.OnMovieListDataReady(movies);
        }
    }


    public void movieTtrailerRequest(String movieId) {
        Retrofit retrofit = new Retrofit.Builder().baseUrl(API_BASE_URL).client(mClient).addConverterFactory(mGsonFactory).build();

        MovieClient moviesClient = retrofit.create(MovieClient.class);

        Call<MovieTrailerListModel> call = moviesClient.getMovieTrailers(movieId, API_KEY);

        // Use call object to make request
        call.enqueue(new Callback<MovieTrailerListModel>() {

            @SuppressWarnings("NullableProblems")
            @Override
            public void onResponse(Call<MovieTrailerListModel> call, Response<MovieTrailerListModel> response) {

                if (response.isSuccessful()) {
                    MovieTrailerListModel movieTrailers = response.body();

                    // Pass back only the MovieList
                    try {
                        //noinspection ConstantConditions
                        notifyMovieTrailerListDataReady(movieTrailers.getResults());
                    } catch (NullPointerException e) {
                        Log.d(TAG, "Network query results cannot be parse into MovieTrailerListModel Object");
                        Log.d(TAG, "Error: " + e.getMessage());
                    }

                } else {
                    Log.d(TAG, "Retrofit Error: " + response.message() + " " + response.errorBody());
                }
            }

            @SuppressWarnings("NullableProblems")
            @Override
            public void onFailure(Call<MovieTrailerListModel> call, Throwable t) {
                Log.d(TAG, "Error: Retrofit call unsucessful....");
                notifyMovieTrailerListFailure(t);
            }
        });

    }

    private void notifyMovieTrailerListFailure(Throwable t) {
        for (ServiceInterface listener : mListeners) {
            listener.OnMovieTrailersDataFailure(t);
        }
    }


    private void notifyMovieTrailerListDataReady(List<MovieTrailerModel> trailers) {
        for (ServiceInterface listener : mListeners) {
            listener.OnMovieTrailersDataReady(trailers);
        }
    }


    public void movieReviewsRequest(String movieId) {
        Retrofit retrofit = new Retrofit.Builder().baseUrl(API_BASE_URL).client(mClient).addConverterFactory(mGsonFactory).build();

        MovieClient moviesClient = retrofit.create(MovieClient.class);

        Call<MovieReviewListModel> call = moviesClient.getMovieReviews(movieId, API_KEY);

        // Use call object to make request
        call.enqueue(new Callback<MovieReviewListModel>() {

            @SuppressWarnings("NullableProblems")
            @Override
            public void onResponse(Call<MovieReviewListModel> call, Response<MovieReviewListModel> response) {

                if (response.isSuccessful()) {
                    MovieReviewListModel movieReviews = response.body();

                    // Pass back only the ReviewList
                    try {
                        //noinspection ConstantConditions
                        notifyMovieReviewListDataReady(movieReviews.getResults());
                    } catch (NullPointerException e) {
                        Log.d(TAG, "Network query results cannot be parse into MovieReviewListModel Object");
                        Log.d(TAG, "Error: " + e.getMessage());
                    }

                } else {
                    Log.d(TAG, "Retrofit Error: " + response.message() + " " + response.errorBody());
                }
            }

            @SuppressWarnings("NullableProblems")
            @Override
            public void onFailure(Call<MovieReviewListModel> call, Throwable t) {
                Log.d(TAG, "Error: Retrofit call unsucessful....");
                notifyMovieTrailerListFailure(t);
            }
        });
    }

    private void notifyMovieReviewListFailure(Throwable t) {
        for (ServiceInterface listener : mListeners) {
            listener.OnMovieReviewDataFailure(t);
        }
    }


    private void notifyMovieReviewListDataReady(List<MovieReviewModel> reviews) {
        for (ServiceInterface listener : mListeners) {
            listener.OnMovieReviewDataReady(reviews);
        }
    }

}
