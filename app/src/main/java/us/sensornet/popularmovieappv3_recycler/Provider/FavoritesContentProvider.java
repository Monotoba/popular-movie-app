package us.sensornet.popularmovieappv3_recycler.Provider;

import android.content.ContentProvider;
import android.content.ContentUris;
import android.content.ContentValues;
import android.content.UriMatcher;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.Log;

import us.sensornet.popularmovieappv3_recycler.Database.FavoritesDbContract;
import us.sensornet.popularmovieappv3_recycler.Database.FavoritesDbHelper;
import us.sensornet.popularmovieappv3_recycler.Database.FavoritesDbWrapper;

public class FavoritesContentProvider extends ContentProvider {
    private static final String TAG = "FavoritesContentProvide";


    public static final int FAVORITES = 100;
    public static final int FAVORITES_WITH_ID = 110;


    private static final UriMatcher uriMatcher = buildUriMatcher();
    private FavoritesDbWrapper dbWrapper;


    public static UriMatcher buildUriMatcher() {
        UriMatcher matcher = new UriMatcher(UriMatcher.NO_MATCH);
        matcher.addURI(FavoritesDbContract.AUTHORITY, FavoritesDbContract.FAVORITES_PATH, FAVORITES);
        matcher.addURI(FavoritesDbContract.AUTHORITY, FavoritesDbContract.FAVORITES_PATH +"/#", FAVORITES_WITH_ID);

        return matcher;
    }


    @Override
    public boolean onCreate() {
        dbWrapper = new FavoritesDbWrapper(getContext(), true);
        return true;
    }

    @Nullable
    @Override
    // Projection: Defines the columns to be returned,
    public Cursor query(@NonNull Uri uri, @Nullable String[] projection, @Nullable String selection, @Nullable String[] selection_args, @Nullable String sort_order) {
        Cursor results = null;

        int match = uriMatcher.match(uri);
        switch(match) {
            case FAVORITES:
                results = dbWrapper.getMovies(projection,selection, selection_args, sort_order);

                results.setNotificationUri(getContext().getContentResolver(), uri);
                break;

            case FAVORITES_WITH_ID:
                String id = uri.getPathSegments().get(1);
                String mSelection = FavoritesDbContract.Movies.COLUMN_MOVIE_ID + "=?";
                String[] mSelectionArgs = new String[]{id};

                results = dbWrapper.getMovies(projection, selection,selection_args, sort_order);

                break;
            default:
                Log.d(TAG, "Unknown URI: " + uri);
        }

        return results;

    }

    @Nullable
    @Override
    public String getType(@NonNull Uri uri) {
        return null;
    }

    @Nullable
    @Override
    public Uri insert(@NonNull Uri uri, @Nullable ContentValues values) {
        Uri result = null;

        int match = uriMatcher.match(uri);

        switch (match) {
            case FAVORITES:
                long id = dbWrapper.insertMovie(values);

                if (id > 0) {

                    result = ContentUris.withAppendedId(FavoritesDbContract.FAVORITES_CONTENT_URI, id);
                    getContext().getContentResolver().notifyChange(uri, null);

                } else {

                    Log.e(TAG, "Insertion failed to: " + uri);
                }
                break;

            default:
                Log.w(TAG, "Unknown URI: " + uri);
        }

        return result;
    }

    @Override
    public int delete(@NonNull Uri uri, @Nullable String selection, @Nullable String[] selectionArgs) {
        int result = 0;

        int match = uriMatcher.match(uri);

        switch (match) {
            case FAVORITES_WITH_ID:
                String id = uri.getPathSegments().get(1);
                result = dbWrapper.removeMovie(id);
                break;
            default:
                Log.w(TAG, "Unknown URI: " + uri);
        }

        if (result > 0) getContext().getContentResolver().notifyChange(uri, null);

        return result;
    }

    @Override
    public int update(@NonNull Uri uri, @Nullable ContentValues values, @Nullable String selection, @Nullable String[] selectionArgs) {
        int results = 0;

        int match = uriMatcher.match(uri);

        switch (match) {
            case FAVORITES_WITH_ID:
                results = dbWrapper.updateMovie(values);
                break;

            default:
                Log.w(TAG, "Unknown URI: " + uri);
        }

        return results;
    }
}
